#########################
## CONSTANTS HAMMER.JS ##
#########################

plugin.tx_hive_thm_jq_hammer {
	settings {
	    lib {
	        hammer {
	            bUseHammer = 1
	        }
	    }
        production {
            includePath {
                public = EXT:hive_thm_jq_hammer/Resources/Public/
                private = EXT:hive_thm_jq_hammer/Resources/Private/
                frontend {
                    public = typo3conf/ext/hive_thm_jq_hammer/Resources/Public/
                }

            }
        }
    }
}